import sys

try:
    curr_version = sys.argv[1]
except IndexError:
    print('USAGE\n\treplace-version.py <version>')
    sys.exit(1)

with open('pyproject.toml') as f:
    lines = f.readlines()    

for i, line in enumerate(lines):
    if line.startswith('version = "'):
        lines[i] = f'version = "{curr_version}"\n'
        break

with open('pyproject.toml', 'w') as f:
    f.writelines(lines)
