## Raw sockets

Raw sockets operate on [OSI layer 2](https://osi-model.com/network-layer/), the Data Link Layer. This layer is also known as the layer in which the Ethernet protocol operates.

??? tip "Seeing `PermissionError` when creating a socket?"
    You need to be root to create raw sockets.
    If not, you'll always receive this error.

    ```pycon
    >>> create_and_bind_socket('lo', 5000)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    File [REDACTED], line 76, in create_and_bind_socket
        s = create_socket()
            ^^^^^^^^^^^^^^^
    File [REDACTED], line 82, in create_socket
        return socket.socket(
            ^^^^^^^^^^^^^^
    File "/usr/lib/python3.11/socket.py", line 232, in __init__
        _socket.socket.__init__(self, family, type, proto, fileno)
    PermissionError: [Errno 1] Operation not permitted
    ```

!!! danger "Windows support"
    Windows currently doesn't allow services to send packets using raw sockets.
    The only functionality that the Windows Socket API has is for receiving data,
    not sending.
    Sending data using IP (or any layer higher) sockets is perfectly fine.

Since raw sockets allow the developer to send any packet with any data directly to the network,
some security concerns may arise.
Because of this, root priviledges are needed to bind to these types of sockets.
IP sockets and above don't have this peculiarity.

### Interacting with raw sockets

```py title="Creating a raw socket"
import socket

sock = socket.socket(
        socket.AF_PACKET, # (1)
        socket.SOCK_RAW,
        socket.PACKET_OTHERHOST
    )
```

1. `AF_PACKET` is a low-level interface directly to network devices ([source](https://docs.python.org/3/library/socket.html)).

The third argument in the socket creation is called `pkttype` and it contains the packet type.
It can be one of the following (taken from [here](https://docs.python.org/3/library/socket.html)):

- `PACKET_HOST` - Packet addressed to the local host.
- `PACKET_BROADCAST` - Physical-layer broadcast packet.
- `PACKET_MULTICAST` - Packet sent to a physical-layer multicast address.
- `PACKET_OTHERHOST` - Packet to some other host that has been caught by a device driver in promiscuous mode.
- `PACKET_OUTGOING` - Packet originating from the local host that is looped back to a packet socket.

The [Linux documentation](https://man7.org/linux/man-pages/man7/packet.7.html) goes a little further in its explanation of the last type, but it's more of the same:

> `sll_pkttype` contains the packet type.  Valid types are
> `PACKET_HOST` for a packet addressed to the local host,
> `PACKET_BROADCAST` for a physical-layer broadcast packet,
> `PACKET_MULTICAST` for a packet sent to a physical-layer
> multicast address, `PACKET_OTHERHOST` for a packet to some other
> host that has been caught by a device driver in promiscuous
> mode, and `PACKET_OUTGOING` for a packet originating from the
> local host that is looped back to a packet socket.  These
> types make sense only for receiving.

The functions `pyllnu.eth.create_socket` or `pyllnu.eth.create_and_bind_socket`
default to `PACKET_OTHERHOST`.

## IP sockets

:construction:

## Higher-level sockets

:construction:
