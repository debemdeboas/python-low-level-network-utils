# [Python Low-Level Network Utils](https://gitlab.com/debemdeboas/python-low-level-network-utils)

<div align="center">
    <img src="https://gitlab.com/debemdeboas/python-low-level-network-utils/-/raw/main/docs/img/wireshark-hello-world.png" />
</div>

PyLLNU implements the following protocols:

- Ethernet
- ICMP
- IPv4
- IPv6
- TCP
- UDP
- DHCP

This package allows developers to interact more easily with binary network sockets and network
protocols.
You can create IPv4 and IPv6 headers, TCP and UDP packets, and even build your own protocols.
